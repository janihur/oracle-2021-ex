Oracle 21 XE Ubuntu Quick Start Guide for Dummies
=================================================

This is not a detailed DBA guide but a quick start guide for developers to get the database up and running locally with reasonable (IMO) defaults.

The easiest way to use the free XE/Express version of Oracle database management system in Ubuntu based Linux distros is the Docker container provided by Oracle. The container can be found from [Oracle's container registry](https://container-registry.oracle.com): 

```
https://container-registry.oracle.com/
 > Category: Database
  > Repository: express
```

The page found above also contains usage instructions but I didn't found those clear enough for noob like me so I have here detailed transcript of the required steps for a Docker and Oracle DBA dummy.

Note that Oracle provides the `latest` Docker tag to get the latest XE version but here we're using explicit version tag `21.3.0-xe`.

I used Lubuntu 22.04 LTS for this guide.

1. Docker on Ubuntu
-------------------

Install:

```
sudo apt install docker.io
```

Note that in Ubuntu Docker requires `sudo` by default but that [can be changed](https://docs.docker.com/engine/install/linux-postinstall/) by adding the user to the `docker` group:

```
sudo usermod -aG docker $USER
```

Basic Docker commands I found useful:

```
# download image
docker pull <IMAGE>

# list all images
docker image ls

# create new (start/run) container from the image
docker run --detach --name <NAME> <IMAGE>

# list all (both running and stopped) containers
docker ps --all

# stop container
docker stop <NAME>

# restart stopped container
docker start <NAME>

# remove container
docker rm <NAME>

# show container logs
docker logs <NAME>

# list container port mappings
docker port <NAME>
```

2. Download the Docker Image
----------------------------

It's huge so might take some time in slow connections:

```
docker pull container-registry.oracle.com/database/express:21.3.0-xe
```

3. Run Oracle With Default Docker Setting
-----------------------------------------

With this method the database can't be accessed outside of the container.

```
docker run \
 --detach \
 --name <NAME> \
 container-registry.oracle.com/database/express:21.3.0-xe
```

Oracle automatically creates random password for `SYS`, `SYSTEM` and `PDBADMIN` users. The password have to be changed to known one:

```
docker exec <NAME> ./setPassword.sh <PASSWORD>
```

Now the database can be accessed by running `sqlplus` inside the container:

```
# connect to CDB$ROOT as sysdba
docker exec --interactive --tty <NAME> sqlplus sys/<PASSWORD>@xe as sysdba

# connect to CDB$ROOT as non-sysdba
docker exec --interactive --tty <NAME> sqlplus system/<PASSWORD>@xe

# connect to the XEPDB1 (pluggable) database
docker exec --interactive --tty <NAME> sqlplus pdbadmin/<PASSWORD>@xepdb1
```

That's nice but even nicer would be to access the database from the outside of the container with your preferred tools !

4. Run Oracle With Reasonable Docker Settings
---------------------------------------------

What I really want to do is to connect to the database from the outside of the Docker container.

We're using [bind mounts](https://docs.docker.com/storage/bind-mounts/) directory `/var/lib/oracle/<NAME>` on the host machine into a container `/opt/oracle/oradata` (`--volume` option in the commands below). Possible data files in the directory will be overwritten as new database is created. The host directory has to exists and be writable by container's `Oracle` user:

```
# create the host directory
sudo mkdir -p /var/lib/oracle/<NAME>

# make the directory writable by container's Oracle-user
sudo chmod a+w /var/lib/oracle/<NAME>
```

The default instructions in Oracle site guide you to use Docker's bridge [network mode](https://docs.docker.com/engine/reference/run/#network-settings) but then you can't use `localhost` in your database connection string but have to figure out the IP address of Docker's default `docker0` network bridge.

Docker option `--env ORACLE_PWD=<PASSWORD>` sets the password for `SYS`, `SYSTEM`and `PDBADMIN` users. Note the password length have to be minimum 8 characters.

Other possible configurations not set here are:

* Setting up ORACLE_CHARACTERSET but I found the default [character set](https://docs.oracle.com/en/database/oracle/oracle-database/21/sqlrf/Character-Set-Support.html) (AL32UTF8) fine.
* Configuring _after setup_ and _on startup_ scripts.

### 4.1 With Docker Network Mode: Host

In this Docker network setup the ports can't be configured but the default values are used:

* Oracle Listener on port 1521 
* Oracle EM Express on port 5500

```
# create and start the container
# will take several minutes as new database is setup to directory /var/lib/oracle/<NAME>
docker run \
 --detach \
 --name <NAME> \
 --network host \
 --env ORACLE_PWD=<PASSWORD> \
 --volume /var/lib/oracle/<NAME>:/opt/oracle/oradata \
 container-registry.oracle.com/database/express:21.3.0-xe
```

### 4.2 With Docker Network Mode: Bridge 

In this Docker network setup the ports are configured but the IP address used to connect the database should be the IP address connected to network bridge `docker0` that is created by Docker by default:

```
$ ip -4 address show dev docker0
3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
```

Here the IP address is `172.17.0.1`.

See e.g. this [SO answer](https://stackoverflow.com/a/24326540/272735) for the details.

```
# create and start the container
# will take several minutes as new database is setup to directory /var/lib/oracle/<NAME>
docker run \
 --detach \
 --name <NAME> \
 --publish 1521:1521 \
 --publish 5500:5500 \
 --env ORACLE_PWD=<PASSWORD> \
 --volume /var/lib/oracle/<NAME>:/opt/oracle/oradata \
 container-registry.oracle.com/database/express:21.3.0-xe
```

### 4.3 Connect the Database

Now I can use Oracle's SQLcl (or SQLDeveloper) to access the database:

```
# sqlcl below is just an handy alias for the actual Oracle's SQLcl executable

# connect to CDB$ROOT as sysdba
sqlcl -noupdates -LOGON sys/<PASSWORD>@//localhost:1521/XE as sysdba

# connect to CDB$ROOT as non-sysdba
sqlcl -noupdates -LOGON system/<PASSWORD>@//localhost:1521/XE

# connect to the XEPDB1 pluggable database
sqlcl -noupdates -LOGON pdbadmin/<PASSWORD>@//localhost:1521/XEPDB1
```

Replace `localhost` above with the IP address if using Docker bridge network mode.

5. Using the Database
---------------------

I'm not going into Oracle DBMS server details here but only show how to use the ready-made `XEPDB1` pluggable database.

```
# connect to CDB$ROOT as sysdba
sqlcl -noupdates -LOGON sys/<PASSWORD>@//localhost:1521/XE as sysdba

SQL> show con_name
CON_NAME
------------------------------
CDB$ROOT
SQL> show pdbs

    CON_ID CON_NAME                       OPEN MODE  RESTRICTED
---------- ------------------------------ ---------- ----------
         2 PDB$SEED                       READ ONLY  NO
         3 XEPDB1                         READ WRITE NO
SQL> alter session set container = xepdb1;

Session altered.

SQL> show con_name
CON_NAME
------------------------------
XEPDB1
SQL>
```

Create database user `FOO`:

```
SQL> create user foo identified by foo;

User FOO created.

SQL> grant create session to foo;

Grant succeeded.

SQL> alter user foo default tablespace users temporary tablespace temp;

User FOO altered.

SQL> grant unlimited tablespace to foo;

Grant succeeded.

SQL> grant create table to foo;

Grant succeeded.
```

Connect as user `FOO`:

```
sqlcl -noupdates -LOGON foo/foo@//localhost:1521/XEPDB1

SQL> show con_name
CON_NAME
------------------------------
XEPDB1
SQL>

SQL> create table a(b number(1));

Table A created.

SQL> insert into a values(1);

1 row inserted.

SQL> commit;

Commit complete.

SQL> select * from a;

         B
----------
         1

SQL>
```

Enjoy !
