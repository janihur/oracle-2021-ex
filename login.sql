-- login.sql
-- SQLcl user login startup file.
--
-- This script is automatically run after glogin.sql
--
-- Located at ${SQLPATH}. Try: show sqlpath
--

-- database date formats
ALTER SESSION SET nls_date_format = 'YYYY-MM-DD HH24:MI:SS';
ALTER SESSION SET nls_timestamp_format = 'YYYY-MM-DD HH24:MI:SS.FF3';
ALTER SESSION SET nls_timestamp_tz_format = 'YYYY-MM-DD HH24:MI:SS.FF3 TZR';

SET SQLPROMPT "_USER'@'_CONNECT_IDENTIFIER> "

-- set highlighting on
-- set highlighting keyword foreground blue
-- set highlighting identifier foreground magenta
-- set highlighting string foreground green
-- set highlighting number foreground cyan
-- set highlighting comment foreground yellow

set highlighting on
set highlighting keyword foreground green
set highlighting identifier foreground magenta
set highlighting string foreground yellow
set highlighting number foreground cyan
set highlighting comment background white
set highlighting comment foreground black 

set serveroutput on

set sqlformat ansiconsole

set statusbar on
set statusbar add editmode 
set statusbar add txn
set statusbar add timing

column sysdate format a19
column global_name format a25
column user format a10
column current_schema format a14
select sysdate, 
       global_name, 
       user, 
       sys_context('userenv', 'current_schema') as current_schema 
  from global_name;
