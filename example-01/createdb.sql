create table orders(
 id number generated always as identity
,customer varchar2(50 char)
,order_date date
);

insert into orders(customer, order_date) values('jani', date'2023-05-25');
insert into orders(customer, order_date) values('jani', date'2023-05-24');
insert into orders(customer, order_date) values('jani', date'2023-05-23');
insert into orders(customer, order_date) values('jani', date'2023-05-22');
insert into orders(customer, order_date) values('jani', date'2023-05-21');

insert into orders(customer, order_date) values('pierre', date'2023-05-23');
insert into orders(customer, order_date) values('pierre', date'2023-05-22');
insert into orders(customer, order_date) values('pierre', date'2023-05-21');
insert into orders(customer, order_date) values('pierre', date'2023-05-20');
insert into orders(customer, order_date) values('pierre', date'2023-05-19');

insert into orders(customer, order_date) values('hugo', date'2023-05-15');
insert into orders(customer, order_date) values('hugo', date'2023-05-14');
insert into orders(customer, order_date) values('hugo', date'2023-05-13');
insert into orders(customer, order_date) values('hugo', date'2023-05-12');
insert into orders(customer, order_date) values('hugo', date'2023-05-11');

