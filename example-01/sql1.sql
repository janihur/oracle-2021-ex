-- https://en.wikipedia.org/wiki/Window_function_(SQL)
with
sorted_rows as(
  select
   row_number() over(partition by customer order by order_date desc) as rn
  ,orders.*
  from orders
)
select * from sorted_rows
where rn <= 2
order by customer, rn
;
