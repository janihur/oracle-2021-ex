select
 customer
,max(order_date) as order_date
,max(id) keep(dense_rank last order by order_date) as id
from orders
group by customer
order by customer
;
