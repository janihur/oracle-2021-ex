/* Create a database user

Usage:

sqlcl -noupdates -LOGON sys/<PASSWORD>@<DATABASE> as sysdba \
 @create-user.sql <USERNAME> <PASSWORD>
*/

define p_username=&&1
define p_password=&&2

/* Default profile password limits

select resource_name, limit from dba_profiles where profile = 'DEFAULT';

alter profile DEFAULT limit password_life_time 365; -- or unlimited
alter profile DEFAULT limit password_grace_time 30;
*/

/* How to open expired and locked accounts

col username for a16
col profile for a7
col account_status for a16
select username, profile, account_status, lock_date, expiry_date from dba_users;

-- unexpire an account by resetting the password
alter user <USER> identified by <PASSWD>;

-- unlock
alter user <USER> account unlock;

*/

create user &&p_username identified by &&p_password;

alter user &&p_username default tablespace users temporary tablespace temp;

grant create session to &&p_username;
grant unlimited tablespace to &&p_username;
grant create table to &&p_username;
grant create sequence to &&p_username;

grant create procedure to &&p_username;

grant create database link to &&p_username;
grant create public database link to &&p_username;

grant create view to &&p_username;

grant create type to &&p_username;
grant create synonym to &&p_username;

grant execute on dbms_hprof to &&p_username;
grant execute on utl_file to &&p_username;

/* Data pump */
grant read, write on directory data_pump_dir to &&p_username;

grant create trigger to &&p_username;

/* DBMS_SCHEDULER */
grant create job to &&p_username;

grant create any context to &&p_username;
grant drop any context to &&p_username;

/* debug */
grant debug connect session to &&p_username;
grant debug any procedure to &&p_username;

begin
  dbms_network_acl_admin.append_host_ace(
    host => '127.0.0.1',
    lower_port => null,
    upper_port => null,
    ace  =>  xs$ace_type(
      privilege_list => xs$name_list('jdwp'),
      principal_name => '&&p_username',
      principal_type => xs_acl.ptype_db
    )
  );
end;
/
