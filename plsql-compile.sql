select * from user_objects
where object_type = 'TRIGGER'
order by object_name
;

alter trigger <TRIGGER_NAME> compile reuse settings;
show errors

-- -----------------------------------------------------------------------------

select * from user_objects
where object_type in ('PACKAGE', 'PACKAGE BODY')
order by object_name, object_type
;

alter package <PACKAGE_NAME> compile      reuse settings;
show errors
alter package <PACKAGE_NAME> compile body reuse settings;
show errors
