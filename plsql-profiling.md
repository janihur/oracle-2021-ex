How to Profile PL/SQL Code Execution Times
==========================================

Oracle has PL/SQL Hierarchical Profiler since 11g. See Oracle documentation:

Oracle 18 Database Development Guide
https://docs.oracle.com/en/database/oracle/oracle-database/18/adfns/
Chapter 15 Using the PL/SQL Hierarchical Profiler 
https://docs.oracle.com/en/database/oracle/oracle-database/18/adfns/hierarchical-profiler.html

Profiler Setup
--------------

See Database Development Guide.

In the examples below the following database directory object is assumed:

```
PLSHPROF_DIR = /tmp/plshprof
```

How to Run Profiler
-------------------

```
declare
  v_profiler_output constant varchar2(32767) := 
    'profiler-' || to_char(sysdate, 'YYYYMMDD-HH24MI') || '.trc';
begin
  -- writes raw profiler output to file: PLSHPROF_DIR/profiler.trc
  dbms_hprof.start_profiling('PLSHPROF_DIR', v_profiler_output);

  <PL/SQL TO BE PROFILED>

  dbms_hprof.stop_profiling;
end;
/
```

Profiling Reports
-----------------

The raw profiler output can be analyzed and saved either to a database tables
or to an HTML report. I have always found HTML report enough.

Generate HTML Report
--------------------

```
plshprof -output <REPORT> <RAW_PROFILER_OUTPUT_FILE>
```

Oracle 18 XE Docker Cheat Sheet
-------------------------------

Create filesystem directory:
```
$ sudo docker exec --interactive --tty <CONTAINER_NAME> /bin/bash
# mkdir /opt/oracle/oradata/plshprof
# chown oracle.oinstall /opt/oracle/oradata/plshprof
```

Grant profiler execution rights (as sysdba):
```
grant execute on dbms_hprof to <USER>;
```

Create database directory object (as a sysdba):
```
create directory plshprof_dir as '/opt/oracle/oradata/plshprof';
grant read, write on directory plshprof_dir to <USER>;
```
