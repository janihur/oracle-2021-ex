Oracle 18 XE Ubuntu Quick Start Guide for Dummies
=================================================

This is not a detailed DBA guide but a quick start guide for developers to get the database up and running locally with reasonable (IMO) defaults.

The easiest way to use the free XE/Express version of Oracle database management system in Ubuntu based Linux distros is the Docker container provided by Oracle. The container can be found from Oracle's container registry: 

```
https://container-registry.oracle.com/
 > Database
  > express
```

The page found above also contains usage instructions but I didn't found those clear enough for noob like me so I have here detailed transcript of the required steps for a Docker and Oracle DBA dummy.

Note that Oracle provides the `latest` Docker tag to get the latest XE version but here we're using explicit version tag `18.4.0-xe`.

I used Lubuntu 20.04 LTS for this guide.

1. Docker on Ubuntu
-------------------

Install:

```
sudo apt install docker.io
```

Basic Docker commands I found useful:

```
# download image
sudo docker pull <IMAGE>

# list all images
sudo docker image ls

# create new (start/run) container from the image
sudo docker run --detach --name <NAME> <IMAGE>

# list all (both running and stopped) containers
sudo docker ps --all

# stop container
sudo docker stop <NAME>

# restart stopped container
sudo docker start <NAME>

# remove container
sudo docker rm <NAME>

# show container logs
sudo docker logs <NAME>

# list container port mappings
sudo docker port <NAME>
```

Note that in Ubuntu one have to always use `sudo` with Docker.

2. Download the Docker Image
----------------------------

It's huge so might take some time in slowly connections:

```
sudo docker pull container-registry.oracle.com/database/express:18.4.0-xe
```

3. Run Oracle With Default Docker Setting
-----------------------------------------

With this method the database can't be accessed outside of the container.

```
sudo docker run \
 --detach \
 --name <NAME> \
 container-registry.oracle.com/database/express:18.4.0-xe
```

The passwords for `SYS`, `SYSTEM` and `PDBADMIN` are created automatically during the container setup and can be found from the logs:

```
$ sudo docker logs <NAME> | head 1
ORACLE PASSWORD FOR SYS AND SYSTEM: <PASSWORD>
```

Now the database can be accessed by running `sqlplus` inside the container:

```
# connect to CDB$ROOT as sysdba
sudo docker exec --interactive --tty <NAME> sqlplus sys/<PASSWORD>@xe as sysdba

# connect to CDB$ROOT as non-sysdba
sudo docker exec --interactive --tty <NAME> sqlplus system/<PASSWORD>@xe

# connect to the XEPDB1 (pluggable) database
sudo docker exec --interactive --tty <NAME> sqlplus pdbadmin/<PASSWORD>@xepdb1
```

That's nice but even nicer would be to access the database from the outside of the container with your preferred tools !

4. Run Oracle With Reasonable Docker Settings
---------------------------------------------

What I really want to do is to connect the database from the outside of the Docker container.

The image has several configuration points that you can tailor for your needs. In the command below:

* `--publish 1521:1521` exposes Oracle Listener port.
* `--publish 5500:5500` exposes Oracle EM that should be accessed https://localhost:5500/em/ but I just run into following error with the latest Firefox: "Secure Connection Failed - An error occurred during a connection to localhost:5500. PR_END_OF_FILE_ERROR". I'm not going to dig deeper.
* `--env ORACLE_PWD=<PASSWORD>` sets the password for `SYS`, `SYSTEM`and `PDBADMIN` users.
* `--volume /var/lib/oracle/<NAME>:/opt/oracle/oradata` [bind mounts](https://docs.docker.com/storage/bind-mounts/) directory `/var/lib/oracle/<NAME>` on the host machine into a container `/opt/oracle/oradata`. Note the host directory has to exists and be writable by container's `Oracle` user.

```
# create the host directory
sudo mkdir -p /var/lib/oracle/<NAME>

# make the directory writable by container's Oracle-user
sudo chmod a+w /var/lib/oracle/<NAME>

# start the container
sudo docker run \
 --detach \
 --name <NAME> \
 --publish 1521:1521 \
 --publish 5500:5500 \
 --env ORACLE_PWD=<PASSWORD> \
 --volume /var/lib/oracle/<NAME>:/opt/oracle/oradata \
 container-registry.oracle.com/database/express:18.4.0-xe
```

Other possible configurations are:

* Setting up ORACLE_CHARACTERSET but I found the default (AL32UTF8) fine.
* Configuring setup and startup scripts.

Now I can use Oracle's SQLcl (or SQLDeveloper) to access the database:

```
# sqlcl below is just an handy alias for the actual Oracle's SQLcl executable

# connect to CDB$ROOT as sysdba
sqlcl -noupdates -LOGON sys/<PASSWORD>@//localhost:1521/XE as sysdba

# connect to CDB$ROOT as non-sysdba
sqlcl -noupdates -LOGON system/<PASSWORD>@//localhost:1521/XE

# connect to the XEPDB1 pluggable database
sqlcl -noupdates -LOGON pdbadmin/<PASSWORD>@//localhost:1521/XEPDB1
```

5. Using the Database
---------------------

I'm not going into Oracle DBMS server details here but only show how to use the ready-made `XEPDB1` pluggable database.

```
# connect to CDB$ROOT as sysdba
sqlcl -noupdates -LOGON sys/<PASSWORD>@//localhost:1521/XE as sysdba

SQL> show con_name
CON_NAME
------------------------------
CDB$ROOT
SQL> show pdbs

    CON_ID CON_NAME                       OPEN MODE  RESTRICTED
---------- ------------------------------ ---------- ----------
         2 PDB$SEED                       READ ONLY  NO
         3 XEPDB1                         READ WRITE NO
SQL> alter session set container = xepdb1;

Session altered.

SQL> show con_name
CON_NAME
------------------------------
XEPDB1
SQL>
```

Create database user `FOO`:

```
SQL> create user foo identified by foo;

User FOO created.

SQL> grant create session to foo;

Grant succeeded.

SQL> alter user foo default tablespace users temporary tablespace temp;

User FOO altered.

SQL> grant unlimited tablespace to foo;

Grant succeeded.

SQL> grant create table to foo;

Grant succeeded.
```

Connect as user `FOO`:

```
sqlcl -noupdates -LOGON foo/foo@//localhost:1521/XEPDB1

SQL> show con_name
CON_NAME
------------------------------
XEPDB1
SQL>

SQL> create table a(b number(1));

Table A created.

SQL> insert into a values(1);

1 row inserted.

SQL> commit;

Commit complete.

SQL> select * from a;

         B
----------
         1

SQL>
```

Enjoy !
